const { User } = require('../models');

module.exports =  {
    async createUser(req, res) {
        const { name, email, role } = req.body;
        
        try {
        const user =  await User.create({ name, email, role });

        return res.json(user);
        } catch (error) {
            console.error(error.message);
            return res.status(400).json(error);
        };
    },

    async getUsers(req, res) {
    
        try {
            const users = await User.findAll();

        return res.json(users);
        
        } catch (error) {
            console.error(error.message);
            return res.status(500).json(error);
        };
    },

    async findOneUser(req, res) {
        const uuid = req.params.uuid;
        try {
            const user = await User.findOne({
                where: {uuid},
                include: 'posts'
            });

            return res.json(user);
        } catch (error) {
            console.error(error.message);
            return res.status(500).json(error);
        };
    },

    async deleteUser(req, res) {
        const uuid = req.params.uuid;
        try {

            await User.destroy({
                where: {uuid: uuid}
            });

            return res.json({ message: 'User deleted!'});
        } catch (error) {
            console.error(error.message);
            return res.status(500).json(error);
        };
    },

    async updateUsers(req, res) {
        const uuid = req.params.uuid;
        const { name, email, role } = req.body;
        try {
            const user = await Post.findOne({
                where: { uuid: uuid}  
            });

            user.name = name;
            user.email = email;
            user.role = role;

            await user.save();

            return res.json(user);
        } catch (error) {
            console.error(error.message);
            return res.status(500).json(error);
        };
    }
};